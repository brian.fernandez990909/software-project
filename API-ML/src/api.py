from flask import Flask, request
from flask_cors import CORS

app = Flask(__name__)

CORS(app)

@app.route("/")
def index():
    return '<h1>HELLO WORLD</h1>'

@app.route("/users", methods=['POST'])
def getUsers():
    data = request.json
    return data

if __name__ == "__main__":
    app.run(debug=True)